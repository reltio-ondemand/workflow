package com.reltio.workflow.api.actions;

public interface AuthService {
    String getActiveUsername();
}
