package com.reltio.workflow.api.tasks;


public class TaskConstants {
	// variables to decide whether need to send email or not
    public static final String NOTIFICATION_ON_ASSIGNMENT_ENABLED = "notificationOnAssignmentEnabled";
    public static final String NOTIFICATION_ON_COMPLETION_ENABLED = "notificationOnCompletionEnabled";
}
