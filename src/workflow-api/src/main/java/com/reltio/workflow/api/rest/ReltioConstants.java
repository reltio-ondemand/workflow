package com.reltio.workflow.api.rest;

/**
 * Reltio Cloud API constants
 */
public class ReltioConstants {
    public static final String FAILED = "failed";
    public static final String OK = "OK";
    public static final String SUCCESS = "success";

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String CHANGE_REQUEST_URI_PREFIX = "changeRequests/";
    public static final String RELTIO_REST_API_URL = "/reltio/api/";
    public static final String OBJECT_URIS = "objectURIs";
    public static final String ACCESS_TYPES = "accessTypes";
    public static final String RELTIO_USER = "reltioUser";

    public static final String ENTITY_URI_PREFIX = "entities/";
    public static final String RELATION_URI_PREFIX = "relations/";
}
