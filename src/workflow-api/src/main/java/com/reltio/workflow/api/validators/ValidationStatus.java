package com.reltio.workflow.api.validators;

public enum ValidationStatus {
    CORRECT, INCORRECT
}
