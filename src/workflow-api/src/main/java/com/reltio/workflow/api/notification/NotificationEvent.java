package com.reltio.workflow.api.notification;

/**
 * Enum for different event types
 */
public enum NotificationEvent {
    ASSIGN,
    COMPLETE
}