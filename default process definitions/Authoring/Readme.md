# Data Change Request Review

### Overview
Authoring is a process for reviewing Data Change Requests initiated for Reltio profiles from Authoring screens. As a result of review, a DCR can be approved (which results in an Apply DCR operation) or rejected (which results in a Reject DCR operation).

### Business Process Definition
Out-of-the-box business process definition for Authoring:

![Business Process Definition Diagram](diagram.png)